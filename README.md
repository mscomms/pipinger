# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Description piPinger is a consultancy created rexx script created to ping a list of hosts based on a cron entry then sendingt the result to BES on a multi TCP listener originally by Colin Rice in February 2013 and exended by Mike Smith in July 2017 to handle mutiple instances.
Version 1.1

### How do I get set up? ###

 Installation and setup - single instance
Copy the piPinger.rex to $PPHOME/unsupported/piPinger
Copy the piPinget.list and piPinger.cfg to $PPHOME/cfg
In BES admin console create a MC based on teh multiTCP listener
Add the details from the MC setup to the piPinger.cfg
Add the following to the cron for the ppadmin user
(5 minute ping cycle)
5 * * * * csh -c '$PPHOME/unsupported/piPinger/piPinger.rex -cfg /opt/ISS/POWERpack/cfg/piPinger.cfg' > /dev/null 2>&1

start the MC and normalise the alerts
 Installation and setup - multiple instances
 In this example 3 different pingg frequencys Gole every 5 mins, Silver every 10 minutes, Bronze every 15mins/asiynchronus
 Follow the steps above but create 3 sets of config files called bronze.list, bronze.cfg, silver.list, silver.cfg etc
 edit the 
 
 pinglistfile=/opt/ISS/POWERpack/cfg/piPinger.list
 
 in the cfg files to point to the repective .list files
 
 add the following to the ppadmin cron files
 
0/15 * * * csh -c '$PPHOME/unsupported/piPinger/piPinger.rex -cfg /opt/ISS/POWERpack/cfg/bronze.cfg' > /dev/null 2>&1
set bronze to * * * * * to run asynchronusly (the script will check to see if it is still running and only restert if the previous run has completed)
0/10 * * * * csh -c '$PPHOME/unsupported/piPinger/piPinger.rex -cfg /opt/ISS/POWERpack/cfg/silver.cfg' > /dev/null 2>&1
0/5 * * * * csh -c '$PPHOME/unsupported/piPinger/piPinger.rex -cfg /opt/ISS/POWERpack/cfg/gold.cfg' > /dev/null 2>&1

Logging
all logging is to $PPOG
$PPLOG/piPinger.log 					MC logging
$PPLOG/piPinger_[instance Name].log		Logging for each instance of the piPinger script

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

The original script was written by Colin Rice colin.rice@interlinksoftwaer.com
I have extended the original script to allow multiple insances and to exit if the script instance is still running. mike.smith@interlinksoftware.com  
